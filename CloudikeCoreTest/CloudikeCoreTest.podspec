Pod::Spec.new do |s|
  s.name             = 'CloudikeCoreTest'
  s.version          = '0.1.5'
  s.summary          = 'By far the most fantastic view I have seen in my entire life. No joke.'
 
  s.description      = <<-DESC
This fantastic view changes its color gradually makes your app look fantastic!
                       DESC
 
  s.homepage         = 'https://bitbucket.org/ivanlazarev/cloudikecoretest/'
  s.license          = { :type => 'MIT', :file => 'CloudikeCoreTest/LICENSE' }
  s.author           = { 'Ivan Lazarev' => 'i.s.lazarev@gmail.com' }
  s.source           = { :git => 'https://ivanlazarev@bitbucket.org/ivanlazarev/cloudikecoretest.git', :tag => s.version.to_s }
 
  s.ios.deployment_target = '10.0'
  s.source_files = 'CloudikeCoreTest/CloudikeCoreTest/src/*.{swift}'
 
end
